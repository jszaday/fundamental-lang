export default String

import 'Int'

__native__ {
    #include <string.h>
}

class String {
    __native__ {
        char* buffer;
    }
    
    public String() {
        __native__ {
            self->buffer = "";
        }
    }
    
    public String(other: String) {
        __native__ {
            int i = strlen(other->buffer);
            self->buffer = (char*) calloc(i, sizeof(char));
            strcpy(self->buffer, other->buffer);
        }
    }
    
    public String(buffer: __nativeType__(char*)) {
        __native__ {
            self->buffer = buffer;
        }
    }
    
    public function length(): Int {
        return Int(__nativeValue__(strlen(self->buffer)));
    }
}
