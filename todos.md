Language Todos:
- Find a Better Name
- Determine if Symbols have a Place in XYZ
- Determine if Case Classes have a Place in XYZ -> They Do!!
- Look into Selectors (would this extend Symbols??)
- Define how Packages/Namespaces work
- Define how Exceptions work
- Define how Annotations work

Rain (Compiler) Todos:
- Null Safe and Elvis Operator -> If's as lambdas (part of a series on Better Functions)
- Implement Generics
- Implement Getters/Setters
- Implement Access Modifiers
- Implement a Default List Type
- Implement Functions as Objects
- Implement the Pipeline Operator
- Implement the Get/Invoke Notation
- Implement Any/None/Object/Undefined
- Implement Optional/Variadic Arguments
- Implement "AssignableFrom" under Type
- Implement Inheritance with Parent Searching and Abstract Fields
- Implement Multiple Return Values (Not sure if this will make it into the final revision)
- Implement Booleans and Truthy/Falsey Logic -> Part of Default Object Inheritance
- Determine the Compiler's Behavior when presented with Ambigious Function References
- Investigate How to Implement String Substitution (I think it's do-able, just not going to be easy...)

Standard Library Todos:
- Design Std.Test
- Design Std.Reflection
- Design Std.Exception
- Design Std.Logger
- Design Std.Regex
- Design Std.JSON
- Design Std.Collections.*

# Variadic Templates
class Function(S, T, A...) {
    
    private fn: N->LFunction;
    
    public Function(fn: N->LFunction) {
        @fn = fn
    }
    
    # Translate to a Tuple of Arguments over here:
    public function apply(S self, A args): T {
        ...
    }
}