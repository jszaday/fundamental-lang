#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common/LClass.h"

// BEGIN
LClass* kInt;
LClass* kString;
LClass* kLinkedList;
LObject* Int_init(LObject* self, void** argv, uint32_t argc) {
	int32_t* __value434 = (int32_t*)malloc(sizeof(int32_t));
	*__value434 = 0;
	LObject_set(self, "value", __value434);
	return self;
}
LObject* Int_initWithI(LObject* self, void** argv, uint32_t argc) {
	int32_t* nPtr = (int32_t*) argv[0];
	LObject_set(self, "value", nPtr);
	return self;
}
LObject* Int_plusWithInt(LObject* self, void** argv, uint32_t argc) {
	LObject* other = (LObject*) argv[0];
	int32_t* ourVal = LObject_get(self, "value");
	int32_t* otherVal = LObject_get(other, "value");
	int32_t* __value253 = (int32_t*)malloc(sizeof(int32_t));
	*__value253 = *ourVal + *otherVal;
	return LObject_invoke(LClass_instantiate(kInt), "initWithI", __value253, NULL);
}
LObject* Int_minusWithInt(LObject* self, void** argv, uint32_t argc) {
	LObject* other = (LObject*) argv[0];
	int32_t* ourVal = LObject_get(self, "value");
	int32_t* otherVal = LObject_get(other, "value");
	int32_t* __value071 = (int32_t*)malloc(sizeof(int32_t));
	*__value071 = *ourVal - *otherVal;
	return LObject_invoke(LClass_instantiate(kInt), "initWithI", __value071, NULL);
}
LObject* Int_compareToWithInt(LObject* self, void** argv, uint32_t argc) {
	LObject* other = (LObject*) argv[0];
	return LObject_invoke(self, "minusWithInt", other, NULL);
}
LObject* Int_toString(LObject* self, void** argv, uint32_t argc) {
	int32_t* ourVal = LObject_get(self, "value");
	int32_t* __value149 = (int32_t*)malloc(sizeof(int32_t));
	*__value149 = snprintf(NULL, 0, "%d", *ourVal) + 1;
	int32_t* length = __value149;
	char* buffer = malloc(*length*sizeof(char));
	snprintf(buffer, *length, "%d", *ourVal);
	return LObject_invoke(LClass_instantiate(kString), "initWithC", buffer, NULL);
}
LClass* generateInt() {
	LClass* lClass = LClass_alloc("Int", 7);
	lClass->fields[0]->type = NULL;
	lClass->fields[0]->name = "value";
	lClass->fields[0]->defaultValue = NULL;
	lClass->fields[1]->type = NULL;
	lClass->fields[1]->name = "init";
	lClass->fields[1]->defaultValue = &Int_init;
	lClass->fields[2]->type = NULL;
	lClass->fields[2]->name = "initWithI";
	lClass->fields[2]->defaultValue = &Int_initWithI;
	lClass->fields[3]->type = NULL;
	lClass->fields[3]->name = "plusWithInt";
	lClass->fields[3]->defaultValue = &Int_plusWithInt;
	lClass->fields[4]->type = NULL;
	lClass->fields[4]->name = "minusWithInt";
	lClass->fields[4]->defaultValue = &Int_minusWithInt;
	lClass->fields[5]->type = NULL;
	lClass->fields[5]->name = "compareToWithInt";
	lClass->fields[5]->defaultValue = &Int_compareToWithInt;
	lClass->fields[6]->type = NULL;
	lClass->fields[6]->name = "toString";
	lClass->fields[6]->defaultValue = &Int_toString;
	return lClass;
}
LObject* String_init(LObject* self, void** argv, uint32_t argc) {
	LObject_set(self, "buffer", "");
	return self;
}
LObject* String_initWithC(LObject* self, void** argv, uint32_t argc) {
	char* nPtr = (char*) argv[0];
	LObject_set(self, "buffer", nPtr);
	return self;
}
LObject* String_length(LObject* self, void** argv, uint32_t argc) {
	char* ourBuffer = LObject_get(self, "buffer");
	int32_t* __value438 = (int32_t*)malloc(sizeof(int32_t));
	*__value438 = strlen(ourBuffer);
	return LObject_invoke(LClass_instantiate(kInt), "initWithI", __value438, NULL);
}
LClass* generateString() {
	LClass* lClass = LClass_alloc("String", 4);
	lClass->fields[0]->type = NULL;
	lClass->fields[0]->name = "buffer";
	lClass->fields[0]->defaultValue = NULL;
	lClass->fields[1]->type = NULL;
	lClass->fields[1]->name = "init";
	lClass->fields[1]->defaultValue = &String_init;
	lClass->fields[2]->type = NULL;
	lClass->fields[2]->name = "initWithC";
	lClass->fields[2]->defaultValue = &String_initWithC;
	lClass->fields[3]->type = NULL;
	lClass->fields[3]->name = "length";
	lClass->fields[3]->defaultValue = &String_length;
	return lClass;
}
LObject* LinkedList_init(LObject* self, void** argv, uint32_t argc) {
	LObject_set(self, "head", NULL);
	return self;
}
LObject* LinkedList_addWithLObject(LObject* self, void** argv, uint32_t argc) {
	LObject* obj = (LObject*) argv[0];
	if (LObject_get(self, "head")) {
        LObject* oldHead = LObject_get(self, "head");
        LObject_set(obj, "__next", oldHead);
        LObject_set(self, "head", obj);
    } else {
        LObject_set(self, "head", obj);
        LObject_set(obj, "__next", NULL);
    }
}
LObject* LinkedList_getWithInt(LObject* self, void** argv, uint32_t argc) {
	LObject* i = (LObject*) argv[0];
	LObject* next = LObject_get(self, "head");
	LObject* j = LObject_invoke(LClass_instantiate(kInt), "init", NULL);
	while ((*(int32_t*)LObject_get(LObject_invoke(j, "compareToWithInt", i, NULL), "value")) < 0) {
        next = LObject_get(next, "__next");
        int32_t* __value127 = (int32_t*)malloc(sizeof(int32_t));
        *__value127 = 1;
        j = LObject_invoke(j, "plusWithInt", LObject_invoke(LClass_instantiate(kInt), "initWithI", __value127, NULL), NULL);
    }
	return next;
}
LClass* generateLinkedList() {
	LClass* lClass = LClass_alloc("LinkedList", 4);
	lClass->fields[0]->type = NULL;
	lClass->fields[0]->name = "head";
	lClass->fields[0]->defaultValue = NULL;
	lClass->fields[1]->type = NULL;
	lClass->fields[1]->name = "init";
	lClass->fields[1]->defaultValue = &LinkedList_init;
	lClass->fields[2]->type = NULL;
	lClass->fields[2]->name = "addWithLObject";
	lClass->fields[2]->defaultValue = &LinkedList_addWithLObject;
	lClass->fields[3]->type = NULL;
	lClass->fields[3]->name = "getWithInt";
	lClass->fields[3]->defaultValue = &LinkedList_getWithInt;
	return lClass;
}
// END

int main(void) {
	kString = generateString();
	kInt = generateInt();
	kLinkedList = generateLinkedList();

	LObject* myList = LClass_instantiate(kLinkedList);

	LObject* myString = LClass_instantiate(kString);

	LObject_invoke(myString, "initWithC", "Hello, world!", NULL);

	LObject_invoke(myList, "addWithLObject", myString, NULL);

	LObject* length = LObject_invoke(myString, "length", NULL);

	LObject_invoke(myList, "addWithLObject", length, NULL);

	LObject* lengthAsString = LObject_invoke(length, "toString", NULL);

	LObject_invoke(myList, "addWithLObject", lengthAsString, NULL);

	int32_t* indexPtr = (int32_t*)malloc(sizeof(int32_t));
	*indexPtr = 2;
	LObject* index = LObject_invoke(LClass_instantiate(kInt), "initWithI", indexPtr, NULL);

	LObject* shouldBeHelloWorld = LObject_invoke(myList, "getWithInt", index, NULL);

	printf((char*)LObject_get(shouldBeHelloWorld, "buffer"));

	return 0;
}