# Single-line comments start with a Hashtag

#-
 - Multi-line comments look like this
 -#

# Printing basics
Console.write("Hello,");
Console.writeLn(" World!");

# Declaring values is done using either var or val.
# val declarations are immutable, whereas vars are mutable. Immutability is
# a good thing.
val x = 10 # x is now 10
x = 20     # error: reassignment to val
var y = 10
y = 20     # y is now 20

# Fundamental is statically typed but pretty good at type inference
# You can manually add types like so:
val z: Int = 10
val a: Double = 1.0

# Notice automatic conversion from Int to Double, result is 10.0, not 10
val b: Double = 10

# Boolean values
true
false

# Boolean operations
!true         # false
!false        # true
true == false # false
10 > 5        # true

# Math is as per usual
1 + 1   # 2
2 - 1   # 1
5 * 3   # 15
6 / 2   # 3
6 / 4   # 1
6.0 / 4 # 1.5

# Functions are defined like so:
function sumOfSquares(x: Int, y: Int): Int {
  return (x * x) + (y * y);
}

# Which is equivalent to:
val sum = (x: Int, y:Int): Int -> {
  return x + y;
}

# In most cases (with recursive functions the most notable exception), function
# return type can be omitted, and the same type inference we saw with variables
# will work with function return values:
val square = (x: Int) -> x * x

# Fundamental has standard, imperative flow control methods
for (var x = 1; x <= 4; x += 1) {
  Console.writeLn(x);
}

# As well as functional techniques
(5 to 1 by -1).forEach(Console.writeLn)

# Fundamental's while, until, do/while and do/until work as expected

# Fundamental has standard conditionals
val someCondition = true;
if (someCondition) {
  Console.writeLn("Do something.")
} else {
  Console.writeLn("Do something else.")
}

# as well as single line versions (note, unless is simply inverse if)
val text = unless (someCondition) "nope" else "yeah"

#-
 - Object Oriented Programming
 -#

class Dog {
  # You can have simple fields
  public var breed: String

  # As well as methods
  public function bark() {
    return "Woof, woof!"
  }

  public constructor(breed: String) {
    @breed = breed
  }

  # Backing store with no default value
  private var name_: String = none

  # For a computed property
  public var name: String {
    get {
      return name_;
    }
    set {
      name_ = value;
    }
  }
}

# Look mom, no new keyword!
var fido = Dog("greyhound")
# And computed properties work just like regular ones
fido.name = "Fido"
Console.writeLn(fido.name)    # -> "Fido"
# ... See?
Console.writeLn(fido.breed)   # -> "greyhound"
Console.writeLn(fido.bark())  # -> "Woof, woof!"
