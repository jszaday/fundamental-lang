// Generated from C:/Users/jszaday/Workspace/Fundamental/src/main/antlr\Fundamental.g4 by ANTLR 4.5.3
package in.astralra.fundamental.generated;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link FundamentalParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface FundamentalVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(FundamentalParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(FundamentalParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#classDefinition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassDefinition(FundamentalParser.ClassDefinitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#classDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassDeclaration(FundamentalParser.ClassDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#declarationModifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclarationModifier(FundamentalParser.DeclarationModifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(FundamentalParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#ifStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfStatement(FundamentalParser.IfStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#elseStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElseStatement(FundamentalParser.ElseStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#whileStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileStatement(FundamentalParser.WhileStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#constructor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstructor(FundamentalParser.ConstructorContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#returnStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturnStatement(FundamentalParser.ReturnStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaration(FundamentalParser.DeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#functionLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionLiteral(FundamentalParser.FunctionLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#functionArgumentList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionArgumentList(FundamentalParser.FunctionArgumentListContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment(FundamentalParser.AssignmentContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#assignmentOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignmentOperator(FundamentalParser.AssignmentOperatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#primaryExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimaryExpression(FundamentalParser.PrimaryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#postfixExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostfixExpression(FundamentalParser.PostfixExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#argumentExpressionList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgumentExpressionList(FundamentalParser.ArgumentExpressionListContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#unaryExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryExpression(FundamentalParser.UnaryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#unaryOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryOperator(FundamentalParser.UnaryOperatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#infixExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInfixExpression(FundamentalParser.InfixExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplicativeExpression(FundamentalParser.MultiplicativeExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#additiveExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdditiveExpression(FundamentalParser.AdditiveExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#shiftExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitShiftExpression(FundamentalParser.ShiftExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#relationalExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelationalExpression(FundamentalParser.RelationalExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#equalityExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqualityExpression(FundamentalParser.EqualityExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#andExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAndExpression(FundamentalParser.AndExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#exclusiveOrExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExclusiveOrExpression(FundamentalParser.ExclusiveOrExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#inclusiveOrExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInclusiveOrExpression(FundamentalParser.InclusiveOrExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#logicalAndExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalAndExpression(FundamentalParser.LogicalAndExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#logicalOrExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalOrExpression(FundamentalParser.LogicalOrExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#conditionalExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConditionalExpression(FundamentalParser.ConditionalExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(FundamentalParser.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#exportExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExportExpr(FundamentalParser.ExportExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#importExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImportExpr(FundamentalParser.ImportExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#typeParameters}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeParameters(FundamentalParser.TypeParametersContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#boundedType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoundedType(FundamentalParser.BoundedTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#typeList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeList(FundamentalParser.TypeListContext ctx);
	/**
	 * Visit a parse tree produced by {@link FundamentalParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(FundamentalParser.TypeContext ctx);
}