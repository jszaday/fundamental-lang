package in.astralra.fundamental.type;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

/**
 * Created by jszaday on 7/25/2016.
 */
public class RainFunction extends RainBlock implements RainDeclaration {

    private String name;
    private String extName;
    private RainDeclarationImpl[] arguments;
    private RainType returnType;
    private int flags;

    public RainFunction(RainScope parent, RainType returnType, String name, RainDeclarationImpl... arguments) {
        super(parent);

        this.name = name;
        this.arguments = arguments;
        this.returnType = returnType;
        this.flags = RainAccessModifier.FINAL.getFlag();

        // TODO add arguments count assertion
        for (int i = 0; i < arguments.length; i++) {
            RainDeclarationImpl variable = arguments[i];
            String expression = String.format("(%s) argv[%d]", variable.getType().getIdentifier(), i);
            variable.setValue(new RainNativeExpression(variable.getType(), expression));
            declare(variable);
        }
    }

    public RainDeclarationImpl[] getArguments() {
        return arguments;
    }

    public RainType getReturnType() {
        if (returnType == null) {
            findReturnType();
        }

        if (returnType instanceof RainReference) {
            return ((RainReference<RainType>) returnType).resolve();
        } else {
            return returnType;
        }
    }

    public void setReturnType(RainType returnType) {
        this.returnType = returnType;
    }

    public String getExternalName() {
        if (extName == null) {
            return (extName = generateExternalName(this));
        } else {
            return extName;
        }
    }

    public void setExternalName(String extName) {
        this.extName = extName;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public RainType getType() {
        return RainNativeType.FUNCTION;
    }

    @Override
    public Optional<RainElement> getValue() {
        return Optional.of(this);
    }

    public boolean argumentsMatch(List<RainType> argsTypes) {
        // TODO Validate Optionals/Varargs
        if (argsTypes.size() != arguments.length) {
            return false;
        }

        for (int i = 0; i < argsTypes.size(); i++) {
            RainType ourType = arguments[i].getType();

            if (!argsTypes.get(i).isAssignableFrom(ourType)) {
                return false;
            }
        }

        return true;
    }

    public boolean matches(RainFunction other, boolean checkReturnType) {
        boolean result = name.equals(other.name);

        if (checkReturnType) {
            result = result && returnType.equals(other.returnType);
        }

        result = result && (arguments.length == other.arguments.length);

        for (int i = 0; i < arguments.length && result; i++) {
            result = arguments[i].equals(other.arguments[i]);
        }

        return result;
    }

    private static String generateExternalName(RainFunction function) {
        StringBuilder stringBuilder = new StringBuilder(function.getName());
        Iterator<String> iterator = Arrays.stream(function.getArguments())
                // Get the types
                .map(RainDeclarationImpl::getType)
                // If it is a Reference, use the simple name, otherwise use "Native" (since we can't really resolve native types)
                .map(RainType::getName)
                .iterator();

        if (!iterator.hasNext()) {
            return stringBuilder.toString();
        }

        // Simply append the first type with the "with" prefix
        stringBuilder.append("With").append(iterator.next());

        // And all proceeding types with the "and" prefix
        while (iterator.hasNext()) {
            stringBuilder.append("And").append(iterator.next());
        }

        return stringBuilder.toString();
    }

    public boolean matches(RainFunction other)  {
        return matches(other, other.returnType != null);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof RainFunction && matches((RainFunction) obj);
    }

    @Override
    public int getFlags() {
        return flags;
    }

    @Override
    public void setFlags(int flags) {
        this.flags = flags;
    }

    private RainType findReturnType() {
        // TODO Make recursive so it searches for, while, if and etc.
        for (RainElement element : listElements()) {
            if (element instanceof RainReturn) {
                RainExpression returnValue = ((RainReturn) element).getValue();
                if (returnValue == null) {
                    returnType = RainVoid.getInstance();
                } else {
                    returnType = returnValue.getType();
                }
                return returnType;
            }
        }

        returnType = RainVoid.getInstance();

        return returnType;
    }
}
