package in.astralra.fundamental.type;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by jszaday on 7/27/2016.
 */
public class RainNativeExpression extends RainExpression {

    private String expression;
    private final Object[] lazyExpressions;

    public RainNativeExpression(RainType type, String expression) {
        super(type);

        this.expression = expression;
        this.lazyExpressions = new Object[0];
    }

    public RainNativeExpression(RainType type, Object... lazyExpressions) {
        super(type);

        this.expression = null;
        this.lazyExpressions = lazyExpressions;
    }

    @Override
    public String toString() {
        if (expression == null) {
            expression = Arrays.stream(lazyExpressions).map(String::valueOf).collect(Collectors.joining());
        }

        return expression;
    }
}
