package in.astralra.fundamental.type;

import java.util.*;

/**
 * Created by jszaday on 7/25/2016.
 */
public class RainClass extends RainScope implements RainType, RainDeclaration {

    private String name;
    private int flags;
    private RainDeclaration selfDeclaration;

    public RainClass(RainClass base, String name) {
        super(base);

        this.name = name;
        this.flags = 0;
        this.selfDeclaration = new RainDeclarationImpl(this, "self");
    }

    @Override
    public List<RainDeclaration> resolve(String name) {
        if (name.startsWith("@")) {
            name = name.substring(1);

            if (name.isEmpty()) {
                return Collections.singletonList(selfDeclaration);
            }
        }

        return super.resolve(name);
    }

    @Override
    public RainClass getSelf() {
        return this;
    }

    @Override
    public String getName() {
        return name;
    }

    // TODO Need to separate Instances from Classes
    @Override
    public RainType getType() {
        return RainNativeType.OBJECT;
    }

    @Override
    public Optional<RainElement> getValue() {
        return Optional.of(this);
    }

    @Override
    public String getIdentifier() {
        return getType().getIdentifier();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RainClass rainClass = (RainClass) o;

        return name != null ? name.equals(rainClass.name) : rainClass.name == null;

    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public boolean needsSemicolon() {
        return false;
    }

    @Override
    public int getFlags() {
        return flags;
    }

    @Override
    public void setFlags(int flags) {
        this.flags = flags;
    }

    @Override
    public boolean isAssignableFrom(RainType type) {
        if (type instanceof RainTypeReference) {
            type = ((RainTypeReference) type).resolve();
        }

        if (this == type || type == RainNativeType.OBJECT || type == RainNativeType.VOID) {
            return true;
        } else {
            return getParent() != null && ((RainClass) getParent()).isAssignableFrom(type);
        }
    }
}
