package in.astralra.fundamental.type;

import java.util.Arrays;
import java.util.Optional;

/**
 * Created by jszaday on 7/29/2016.
 */
public enum RainAccessModifier implements RainElement {
    PUBLIC, PRIVATE, PROTECTED, STATIC,
    FINAL, INTERFACE, ABSTRACT;

    private int flag;

    RainAccessModifier() {
        this.flag = (int) Math.pow(2, ordinal());
    }

    public static Optional<RainAccessModifier> lookup(String val) {
        final String finalVal = val.toUpperCase();
        return Arrays.stream(values())
                .filter(mod -> finalVal.equals(mod.name()))
                .findFirst();
    }

    public int getFlag() {
        return flag;
    }

    public boolean isPresent(int mod) {
        return (mod & flag) != 0;
    }

    @Override
    public boolean needsSemicolon() {
        throw new RuntimeException(name() + " cannot be used on its own!");
    }
}
