package in.astralra.fundamental.type;

/**
 * Created by jszaday on 7/29/2016.
 */
public class RainTypeParameter implements RainType {

    private String name;

    public RainTypeParameter(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getIdentifier() {
        return name;
    }

    @Override
    public boolean needsSemicolon() {
        throw new RuntimeException("Can't be referred to on it's own.");
    }

    @Override
    public boolean isAssignableFrom(RainType type) {
        if (type instanceof RainTypeReference) {
            type = ((RainTypeReference) type).resolve();
        }

        // TODO Determine what is appropriate here.
        return type == this; // || type == RainNativeType.VOID || type == RainNativeType.OBJECT;
    }
}
