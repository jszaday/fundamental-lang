package in.astralra.fundamental.type;

import java.util.Optional;

/**
 * Created by jszaday on 7/25/2016.
 */
public class RainTypeReference implements RainReference<RainType>, RainType {

    private final RainScope scope;
    public String name;
    public RainType resolved;

    public RainTypeReference(RainScope scope, String name) {
        this.name = name;
        this.scope = scope;
    }

    public RainTypeReference(RainType resolved) {
        this(null, null);
        this.resolved = resolved;
    }

    RainScope getScope() {
        return scope;
    }

    @Override
    public RainType resolve() {
        if (resolved == null) {
            Optional<RainType> optional = scope.resolveType(name);

            if (optional.isPresent()) {
                return (resolved = optional.get());
            } else {
                throw new RuntimeException("Could not resolve " + name + "!");
            }
        } else {
            return resolved;
        }
    }

    @Override
    public String getName() {
        return resolve().getName();
    }

    @Override
    public String getIdentifier() {
        return resolve().getIdentifier();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        else if (o == null) return false;
        else return o.equals(resolve());
    }

    @Override
    public int hashCode() {
        int result = scope != null ? scope.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (resolved != null ? resolved.hashCode() : 0);
        return result;
    }

    @Override
    public boolean needsSemicolon() {
        throw new RuntimeException("A type cannot be in the main scope.");
    }

    @Override
    public boolean isAssignableFrom(RainType type) {
        return resolve().isAssignableFrom(type);
    }
}
