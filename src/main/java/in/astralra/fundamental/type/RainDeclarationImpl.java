package in.astralra.fundamental.type;

import java.util.Optional;

/**
 * Created by jszaday on 7/25/2016.
 */
public class RainDeclarationImpl implements RainDeclaration {

    private RainType type;
    private String name;
    private int flags;
    private Optional<RainElement> value;

    public RainDeclarationImpl(RainType type, String name) {
        this(type, name, null);
    }

    public RainDeclarationImpl(RainType type, String name, RainElement value) {
        this.type = type;
        this.name = name;
        this.value = Optional.ofNullable(value);
    }

    @Override
    public RainType getType() {
        return type;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setValue(RainElement value) {
        this.value = Optional.ofNullable(value);
    }

    @Override
    public Optional<RainElement> getValue() {
        return Optional.empty();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RainDeclarationImpl that = (RainDeclarationImpl) o;

        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        return name != null ? name.equals(that.name) : that.name == null;

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean needsSemicolon() {
        return true;
    }

    @Override
    public String toString() {
        String temp = type.getIdentifier() + " " + name;

        if (value.isPresent()) {
            return temp + " = " + value.get();
        } else {
            return temp;
        }
    }

    @Override
    public int getFlags() {
        return flags;
    }

    @Override
    public void setFlags(int flags) {
        this.flags = flags;
    }
}
