package in.astralra.fundamental.type;

/**
 * Created by jszaday on 7/29/2016.
 */
public class RainIf implements RainElement {
    private RainExpression expr;
    private RainElement ifCase;
    private RainElement elseCase;

    public RainIf(RainExpression expr, RainElement ifCase, RainElement elseCase) {
        this.expr = expr;
        this.ifCase = ifCase;
        this.elseCase = elseCase;
    }

    public RainIf(RainExpression expr, RainElement ifCase) {
        this(expr, ifCase, null);
    }

    public RainExpression getExpr() {
        return expr;
    }

    public RainElement getIfCase() {
        return ifCase;
    }

    public RainElement getElseCase() {
        return elseCase;
    }

    @Override
    public boolean needsSemicolon() {
        return false;
    }
}
