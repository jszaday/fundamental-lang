package in.astralra.fundamental.type;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by jszaday on 8/1/16.
 */
public class RainNativeValue extends RainExpression implements RainCompoundElement {

    private final RainScope scope;
    private final String value;
    private final boolean isPointer;
    private final RainElement[] expressions;

    public RainNativeValue(RainScope scope, RainNativeType type, String value, boolean isPointer) {
        super(type);

        this.scope = scope;
        this.value = value;
        this.isPointer = isPointer;

        this.expressions = generateExpressions();
    }

    public RainNativeValue(RainScope scope, RainNativeType type, String value) {
        this(scope, type, value, false);
    }

    private RainElement[] generateExpressions() {
        RainType type = getType();
        if (isPointer) {
            return new RainElement[] {
                    new RainNativeExpression(type, value)
            };
        } else {
            String randomName = scope.getRandomName();

            return new RainElement[] {
                    new RainDeclarationImpl(type, randomName, new RainNativeExpression(type, "(" + type.getIdentifier() + ")malloc(sizeof(" + type.getIdentifier().replaceAll("\\*", "") + "))")),
                    new RainAssignment(new RainNativeExpression(type, "*" + randomName), new RainNativeExpression(getType(), value)),
                    new RainNativeExpression(type, randomName)
            };
        }
    }

    @Override
    public RainElement[] getElements() {
        return expressions;
    }

    @Override
    public String toString() {
        throw new RuntimeException("NOT TO BE USED AS A STANDALONE CLASS!!");
    }
}
