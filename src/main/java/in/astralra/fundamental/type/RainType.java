package in.astralra.fundamental.type;

/**
 * Created by jszaday on 7/25/2016.
 */
public interface RainType extends RainElement {
    String getName();
    String getIdentifier();
    boolean isAssignableFrom(RainType type);
}
