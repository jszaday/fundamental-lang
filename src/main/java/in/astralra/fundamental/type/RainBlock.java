package in.astralra.fundamental.type;

import com.sun.xml.internal.ws.api.server.LazyMOMProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jszaday on 7/25/2016.
 */
public class RainBlock extends RainScope implements RainCompoundElement {
    private List<RainElement> elements;

    public RainBlock(RainScope parent, RainClass self) {
        super(parent, self);

        elements = new ArrayList<>();
    }

    public RainBlock(RainScope parent) {
        this(parent, null);
    }

    public boolean add(RainElement element) {
//        if (element instanceof RainDeclaration) {
//            declare((RainDeclaration) element);
//        }
        return elements.add(element);
    }

    public List<RainElement> listElements() {
        return elements;
    }

    @Override
    public boolean needsSemicolon() {
        return false;
    }

    @Override
    public RainElement[] getElements() {
        return elements.toArray(new RainElement[elements.size()]);
    }
}
