package in.astralra.fundamental.type;

import in.astralra.cenjin.core.Element;

/**
 * Created by jszaday on 8/1/16.
 */
public interface RainCompoundElement extends RainElement {
    RainElement[] getElements();
}
