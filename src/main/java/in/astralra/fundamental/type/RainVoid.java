package in.astralra.fundamental.type;

/**
 * Created by jszaday on 7/29/2016.
 */
public class RainVoid implements RainType {

    private RainVoid() {
    }

    @Override
    public String getName() {
        return "Void";
    }

    @Override
    public String getIdentifier() {
        return "Void";
    }

    @Override
    public boolean needsSemicolon() {
        throw new RuntimeException("Can't Refer to on its own!");
    }

    private static RainVoid singleton;

    public static RainVoid getInstance() {
        if (singleton == null) {
            singleton = new RainVoid();
        }

        return singleton;
    }

    @Override
    public boolean isAssignableFrom(RainType type) {
        return false;
    }
}
