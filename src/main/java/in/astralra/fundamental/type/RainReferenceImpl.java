package in.astralra.fundamental.type;

import java.util.List;

/**
 * Created by jszaday on 7/27/2016.
 */
public class RainReferenceImpl extends RainExpression implements RainReference<RainDeclaration> {

    private final String target;
    private final RainScope scope;
    private RainDeclaration resolved;

    public RainReferenceImpl(RainScope scope, String target) {
        super(null);

        this.target = target;
        this.scope = scope;

        List<RainDeclaration> optional = scope.resolve(target);
        if (optional.isEmpty()) {
            resolved = null;
        } else {
            // NOTE This is just getting the first for now, this might need to be changed later on
            resolved = optional.get(0);
        }
    }

    public RainReferenceImpl(String target, RainDeclaration resolved) {
        super(resolved.getType());

        this.target = target;
        this.resolved = resolved;
        this.scope = null;
    }

    @Override
    public RainDeclaration resolve() {
        if (resolved == null) {
            List<RainDeclaration> optional = scope.resolve(target);
            if (optional.isEmpty()) {
                throw new RuntimeException("Could not resolve " + target + "!");
            } else {
                // NOTE This is just getting the first for now, this might need to be changed later on
                resolved = optional.get(0);

                if (resolved instanceof RainDeclarationImpl) {
                    resolved = null;

                    throw new RuntimeException(target + " referred to before it was declared!");
                }
            }
        }

        return resolved;
    }

    public boolean isField() {
        return scope.isField(resolve());
    }

    @Override
    public RainType getType() {
        return resolve().getType();
    }

    public String getTarget() {
        return target;
    }

    public String getName() {
        return resolve().getName();
    }

    @Override
    public String toString() {
        resolve();
        // TODO look for instances??
        if (scope.isField(resolved)) {
            return "LObject_get(self, \""+resolved.getName()+"\")";
        } else if (resolved == RainNil.NIL.getDeclaration()) {
            return "NULL";
        } else {
            return resolved.getName();
        }
    }
}
