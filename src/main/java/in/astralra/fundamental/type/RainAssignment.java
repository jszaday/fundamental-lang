package in.astralra.fundamental.type;

/**
 * Created by jszaday on 7/25/2016.
 */
public class RainAssignment implements RainElement {
    private final RainOperator operator;
    private RainExpression target;
    private RainExpression value;

    public RainAssignment(RainExpression target, RainOperator operator, RainExpression value) {
        this.target = target;
        this.value = value;
        this.operator = operator;
    }

    public RainAssignment(RainExpression target, RainExpression value) {
        this(target, RainOperator.NONE, value);
    }

    @Override
    public String toString() {
        if (!target.getType().isAssignableFrom(value.getType())) {
            throw new RuntimeException("Type mismatch error, cannot assign " + value.getType() + " to " + target.getType() + ".");
        }

        if (target instanceof RainReferenceImpl) {
            if (((RainReferenceImpl) target).isField()) {
                return "LObject_set(self, \"" + ((RainReferenceImpl) target).getName() + "\", " + value + ")";
            } else {
                return target + " = " + value;
            }
        } else {
            return target + " = " + value;
        }
    }

    @Override
    public boolean needsSemicolon() {
        return true;
    }
}
