package in.astralra.fundamental.type;

import in.astralra.cenjin.core.Function;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by jszaday on 7/25/2016.
 */
public class RainScope {

    private RainScope parent;
    private RainClass self;

    private List<RainType> types;
    private List<RainDeclaration> declarations;
    private Random random = new Random();

    public RainScope(RainScope parent, RainClass self) {
        this.parent = parent;
        this.self = self;
        this.types = new ArrayList<>();
        this.declarations = new ArrayList<>();
    }

    public RainScope(RainScope parent) {
        this(parent, null);
    }

    public List<RainDeclaration> getDeclarations() {
        return declarations;
    }

    public RainClass getSelf() {
        if (self == null && parent != null) {
            return parent.getSelf();
        } else {
            return self;
        }
    }

    public boolean isField(RainDeclaration declaration) {
        RainClass self = getSelf();
        if (self == null || !self.contains(declaration, false)) {
            return false;
        } else if (contains(declaration, false)) {
            return getExactMatch(declaration) == ((RainScope) self).getExactMatch(declaration);
        } else {
            return true;
        }
    }

    private RainDeclaration getExactMatch(RainDeclaration declaration) {
        return declarations.stream().filter(dec -> dec == declaration).findFirst().get();
    }

    public RainScope declare(RainDeclaration declaration, Integer flags) {
        // Update the declaration's flags
        declaration.setFlags(flags | declaration.getFlags());
        // And declare it within this scope
        return declare(declaration);
    }

    public RainScope declare(RainDeclaration declaration) {
        // Sweep only the local scope
        if (contains(declaration, false)) {
            throw new RuntimeException(declaration.getName() + " has already been declared in this scope!");
        } else {
            if (declaration instanceof RainType) {
                types.add((RainType) declaration);
            }

            declarations.add(declaration);
        }

        return this;
    }

    public RainTypeReference referenceType(String name) {
        return new RainTypeReference(this, name);
    }

    public List<RainDeclaration> resolve(String name) {
        // Error if multiple matches!!
        if (name.startsWith("@")) {
            RainClass self = getSelf();

            if (self == null) {
                throw new RuntimeException("Trying to resolve self outside of a class!");
            } else {
                return self.resolve(name);
            }
        }

        List<RainDeclaration> found = declarations.stream()
                .filter(rainDeclaration -> name.equals(rainDeclaration.getName()))
                .collect(Collectors.toList());

        if (parent == null) {
            return found;
        } else {
            List<RainDeclaration> parents = parent.resolve(name);

            // Take all of the non-overridden from the parent's scope and add them to ours
            parents.stream().filter(theirs -> !found.stream()
                    .filter(ours -> {
                        if (ours instanceof RainFunction && theirs instanceof RainFunction) {
                            return !((RainFunction) ours).getExternalName().equals(((RainFunction) theirs).getExternalName());
                        } else {
                            return !ours.getName().equals(theirs.getName());
                        }
                    })
                    .findFirst().isPresent())
                    .forEach(found::add);

            return found;
        }
    }

    Optional<RainType> resolveType(String name) {
        Optional<RainType> found = types.stream().filter(type -> type.getName().equals(name)).findFirst();

        if (!found.isPresent() && parent != null) {
            return parent.resolveType(name);
        } else {
            return found;
        }
    }

    List<RainFunction> resolveFunction(RainType returnType, String name, RainDeclarationImpl... args) {
        RainFunction dummy = new RainFunction(null, returnType, name, args);

        List<RainFunction> found = declarations.stream()
                .filter(declaration -> declaration instanceof RainFunction)
                .map(RainFunction.class::cast)
                .filter(function -> function.matches(dummy))
                .collect(Collectors.toList());

        if (parent != null) {
            List<RainFunction> parents = parent.resolveFunction(returnType, name, args);

            // Take all of the non-overridden from the parent's scope and add them to ours
            parents.stream().filter(theirs -> !found.stream()
                    .filter(ours -> !ours.getExternalName().equals(theirs.getExternalName()))
                    .findFirst().isPresent())
                    .forEach(found::add);

            return found;
        } else {
            return found;
        }
    }

    public boolean contains(RainDeclaration declaration, boolean recursive) {
        boolean result = contains(declaration);
        // If the function is recursive, the scope has a parent and the declaration was not found
        if (!result && recursive && parent != null) {
            // Search the parent
            return parent.contains(declaration);
        } else {
            // Otherwise, just return the result
            return result;
        }
    }

    private boolean contains(RainDeclaration declaration) {
        Stream<RainDeclaration> matching = declarations
                .stream()
                .filter(dec -> declaration.getName().equals(dec.getName()));
        if (declaration instanceof RainFunction) {
            return matching
                    .filter(decl -> !(decl instanceof RainFunction) || ((RainFunction) decl).matches((RainFunction) declaration))
                    .findFirst().isPresent();
        } else {
            return matching.findFirst().isPresent();
        }
    }

    public RainScope leave() {
        return parent;
    }

    public String getRandomName() {
        String name = String.format("__value%03d", random.nextInt(1000));

        if (resolve(name).isEmpty()) {
            return name;
        } else {
            return getRandomName();
        }
    }

    public void addType(RainType rainType) {
        this.types.add(rainType);
    }

    RainScope getParent() {
        return parent;
    }
}
