package in.astralra.fundamental.type;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by jszaday on 7/28/2016.
 */
public class RainMultiReference implements RainReference<List<RainFunction>>, RainTyped {
    private final RainScope scope;
    private Object target;
    private List<RainFunction> rainFunctions;

    public RainMultiReference(List<RainDeclaration> declarations) {
        this.scope = null;
        // TODO Add error checking here.
        this.rainFunctions = declarations.stream().map(RainFunction.class::cast).collect(Collectors.toList());
    }

    public RainMultiReference(RainScope scope, String target) {
        this.target = target;
        this.scope = scope;
    }

    public RainMultiReference(RainScope scope, RainExpression reference) {
        if (reference instanceof RainReferenceImpl) {
            this.target = ((RainReferenceImpl) reference).getTarget();
        } else if (reference instanceof RainConnector) {
            this.target = reference;
        } else {
            throw new RuntimeException("No idea what to do with a(n) " + reference.getType());
        }

        this.scope = scope;
    }

    public Object getTarget() {
        return target;
    }

    boolean wasConstructor;
    RainClass rainClass;

    public boolean isConstructor() {
        resolve();

        return wasConstructor;
    }

    public RainClass whichClass() {
        resolve();

        if (wasConstructor) {
            return rainClass;
        } else {
            return null;
        }
    }

    @Override
    public List<RainFunction> resolve() {
        if (rainFunctions == null) {
            List<? extends RainDeclaration> resolved;

            if (target instanceof String) {
                resolved = scope.resolve((String) target);
            } else if (target instanceof RainConnector) {
                RainTyped typed = ((RainConnector) target).resolve();

                if (typed instanceof RainMultiReference) {
                    resolved = ((RainMultiReference) typed).resolve();
                } else if (typed instanceof RainReferenceImpl) {
                    resolved = Collections.singletonList(((RainReferenceImpl) typed).resolve());
                } else {
                    throw new RuntimeException("No idea what to do with a " + typed.getClass().getSimpleName());
                }
            } else {
                throw new RuntimeException("No idea what to do with a " + target.getClass().getSimpleName());
            }

            if (resolved.isEmpty()) {
                throw new RuntimeException("Could not resolve target(s) " + target);
            } else if (resolved.get(0) instanceof RainClass || resolved.get(0) instanceof RainDeclarationImpl) {
                // TODO Determine if this is an appropriate solution or not.
                if (resolved.get(0) instanceof RainDeclarationImpl) {
                    rainClass = (RainClass) resolved.get(0).getType();
                } else {
                    // Save the class
                    rainClass = (RainClass) resolved.get(0);
                }
                // Resolve the constructors
                resolved = rainClass.resolve("init");
                // Mark it as a constructor
                wasConstructor = true;
                // If it's still empty throw a fit
                if (resolved.isEmpty()) {
                    throw new RuntimeException("Could not resolve target(s) " + target);
                }
            } else {
                wasConstructor = false;
            }

            rainFunctions = resolved.stream()
                    .filter(declaration -> declaration instanceof RainFunction)
                    .map(RainFunction.class::cast)
                    .collect(Collectors.toList());

            if (rainFunctions.isEmpty()) {
                throw new RuntimeException("Could not resolve target(s) " + target);
            } else {
                return rainFunctions;
            }
        } else {
            return rainFunctions;
        }
    }

    @Override
    public RainType getType() {
        return resolve().get(0).getReturnType();
    }

    @Override
    public String toString() {
        return "MultiReferenceTo["+String.valueOf(target)+"]";
    }
}
