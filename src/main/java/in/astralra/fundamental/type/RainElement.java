package in.astralra.fundamental.type;

/**
 * Created by jszaday on 7/25/2016.
 */
public interface RainElement {
    boolean needsSemicolon();
}
