export default Int

__native__ {
  #include <stdint.h>
}

class Int {
  __native__ {
    int32_t value;
  }

  public Int() {
    __native__ {
      self->value = 0;
    }
  }

  public Int(value: __nativeType__(int32_t)) {
    __native__ {
      self->value = value;
    }
  }

  public operator function add(other: Int) {
    return Int(__nativeValue__(other->value + self->value))
  }
}
