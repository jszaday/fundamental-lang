import 'Int'
import 'String'

export default Logger

__native__ {
  #include <stdio.h>
}

class Logger {
  public Logger() {

  }

  public function log(value: Int) {
    __native__ {
      printf("%d\n", value->value);
    }
  }
  
  public function log(value: String) {
    __native__ {
      printf("%s\n", value->buffer);
    }
  }
}
