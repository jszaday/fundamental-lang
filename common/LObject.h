#ifndef FUNDAMENTAL_LOBJECT_H
#define FUNDAMENTAL_LOBJECT_H

// For uint32_t
#include <stdint.h>
// For bool
#include <stdbool.h>
// For Class
#include "LClass.h"

// TODO Make non-recursive versions of set and findField
// TODO Add type validation to set/get, how??

// Forward Declare Class
struct LClass;

typedef struct LObjectNode {
  void* value;
  uint32_t hash;
  // TODO Make finalizing a field actually do something (and a method to "freeze" a field)
  bool finalized;
  struct LObjectNode* next;
} LObjectNode;

typedef struct LObject {
  LObjectNode* first;
  struct LObject* base;
  struct LClass* type;
} LObject;

LObjectNode* LObject_findField(LObject* self, char* name, uint32_t* hash);
void LObject_set(LObject* self, char* name, void* value);
void* LObject_get(LObject* self, char* name);
LObject* LObject_invoke(LObject* self, char* name, ...);

#endif
