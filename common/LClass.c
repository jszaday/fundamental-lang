#include "LClass.h"

#include <stdlib.h>

LClass* LClass_alloc(char* name, uint32_t numFields) {
	LClass* fClass = (LClass*)malloc(sizeof(LClass));
	LField** fields = (LField**)malloc(numFields * sizeof(LField*));

	uint32_t i;
	for (i = 0; i < numFields; i++) {
		fields[i] = (LField*)malloc(sizeof(LField));
	}

	fClass->name = name;
	fClass->base = NULL;
	fClass->fields = fields;
	fClass->numFields = numFields;

	return fClass;
}

struct LObject* LClass_instantiate(LClass* lClass) {
  LObject* instance = malloc(sizeof(LObject));
  LField* curr;
  uint32_t i;

  instance->base = NULL;
  instance->first = NULL;

  for (i = 0; i < lClass->numFields; i++) {
    curr = lClass->fields[i];

    LObject_set(instance, curr->name, curr->defaultValue);
  }

  if (lClass->base) {
    instance->base = LClass_instantiate(lClass->base);
  } else {
    instance->base = NULL;
  }

  instance->type = lClass;

  return instance;
}
